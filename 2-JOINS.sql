-- 1. Mostrar la fecha y número de los pedidos con la descripción, cantidad, precio e importe de los artículos de dichos pedidos 

SELECT p.fecha, p.pednum, pr.descrip, p.cant, pr.precio, p.importe
FROM pedido p
JOIN producto pr
	ON p.prodcod = pr.prodcod
		AND pr.fabcod = p.fabcod;
(30 rows)

-- 2. Como la consulta anterior pero añadiendo 
-- el nombre del representante que tomó el pedido y que además 
-- el importe del pedido sea superior a 25.000.

SELECT ped.fecha, ped.pednum, pro.descrip, ped.cant, pro.precio, ped.importe, rep.nombre
FROM pedido ped
JOIN producto pro
	ON ped.prodcod = pro.prodcod AND pro.fabcod = ped.fabcod
JOIN repventa rep
	ON ped.repcod = rep.repcod
WHERE ped.importe > 25000
ORDER BY rep.nombre;
(4 rows)

-- 3. Mostrar un listado de los representantes con su jefe, es decir, código y nombre de representante y lo mismo para su jefe.

SELECT rep.nombre, jef.nombre as "Jefes"
FROM repventa rep
JOIN repventa jef
ON rep.jefe = jef.repcod;

-- 4. Mostrar todos los representantes 
-- (incluyendo los que no tienen asignada una oficina). 
-- Nombre del representante, puesto, ventas, código de oficina y ciudad.

SELECT nombre, puesto, repventa.ventas, repventa.ofinum, oficina.ciudad
FROM repventa
LEFT JOIN oficina 
	ON repventa.ofinum = oficina.ofinum;

-- 5. Como la 3 pero mostrando todos los representantes.

SELECT rep.nombre, jef.nombre as "Jefes"
FROM repventa rep
LEFT JOIN repventa jef
ON rep.jefe = jef.repcod;

-- 6. Obtener una lista de los pedidos con importes 
-- superiores a 150 euros, mostrando el código del pedido, 
-- el importe, el nombre del cliente que lo solicitó, 
-- el nombre del representante que contactó con él por primera vez 
-- y la ciudad de la oficina donde el representante trabaja.

SELECT ped.pednum, ped.importe, clie.nombre, rep.nombre, ofi.ciudad
FROM pedido ped
LEFT JOIN cliente clie
	ON ped.cliecod = clie.cliecod
LEFT JOIN repventa rep
	ON clie.repcod = rep.repcod
LEFT JOIN oficina ofi
	ON rep.ofinum = ofi.ofinum
WHERE ped.importe > 150
ORDER BY 2;

-- 7  Lista los pedidos tomados durante el mes de octubre del 
-- año 1989 (any modificat perquè surti algun resultat) , 
-- mostrando solamente el número del pedido, su importe, el 
-- nombre del cliente que lo realizó, la fecha y 
-- la descripción del producto solicitado.

SELECT pednum, fecha, pedido.importe, cliente.nombre as "Cliente", 
	producto.descrip as "Producto"
FROM pedido                             
JOIN cliente
ON cliente.cliecod = pedido.cliecod 
JOIN producto
ON pedido.prodcod = producto.prodcod             
WHERE fecha between '1989-10-01' AND '1989-10-31';

-- 8. Obtener una lista de todos los pedidos tomados por representantes de oficinas de la región Este, mostrando solamente el número del pedido, la descripción del producto, nombre del representante que lo tomó y la región de su oficina. (Afegeixo al SELECT el camp region perquè es vegi be i ordeno per representant)



-- 9. Obtener los pedidos tomados en los mismos días en que un nuevo representante fue contratado. Mostrar número de pedido, importe, fecha pedido.

-- 10. Obtener una lista con parejas de representantes y oficinas en donde la cuota del representante es mayor o igual que el objetivo de la oficina, sea o no la oficina en la que trabaja. Mostrar Nombre del representante, cuota del mismo, Ciudad de la oficina, objetivo de la misma.

-- 11. Muestra el nombre, las ventas y la ciudad de la oficina de cada representante de la empresa (cal tenir en compte que hi ha algun representant que no té oficina)

-- 12. Obtener una lista de la descripción de los productos para los que existe algún pedido en el que se solicita una cantidad mayor a las existencias de dicho producto.

-- 13. Lista los nombres de los representantes que tienen una cuota superior a la de su director de oficina. (Mostrar nombre representante, cuota, nombre director y cuota del mismo)

-- 14. Obtener una lista de los representantes que trabajan en una oficina distinta de la oficina en la que trabaja su JEFE (enunciat modificat perquè sigui algo diferent a l’anterior), mostrando también el nombre del director y el código de la oficina donde trabaja cada uno de ellos.

-- 15. El mismo ejercicio anterior, mostrando en lugar de códigos de oficinas, ciudad del representante y su jefe (exercici avançat).

-- 16. Mostrar todos los datos de los representantes que son jefe (evitando repeticiones).

-- 17. Obtener los pedidos tomados en los mismos días en que un nuevo representante fue contratado. Mostrar número de pedido, importe, fecha pedido (y fecha de contrato)
